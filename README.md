This module is intended to replace `asus-nb-wmi` module with an updated version for ASUS G14 and G15 laptops.

The preferred way to build and install the module is using dkms.

```
$ make dkms
$ make onboot
```

`make onboot` will deny `asus__nb_wmi` module from loading, `asus_rog_nb_wmi` replaces it.
