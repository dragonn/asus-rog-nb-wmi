%if %{defined fedora}
%global debug_package %{nil}
%endif

%define modname	asus-rog-nb-wmi

Name:           dkms-%{modname}
Version:        0.1.0
Release:        0
Summary:        Asus wmi device driver
License:        GPL-2.0
Group:          System Environment/Kernel
URL:            https://gitlab.com/asus-linux/asus-rog-nb-wmi
Source:         https://gitlab.com/asus-linux/asus-rog-nb-wmi/-/archive/%version/%{modname}-%{version}.tar.gz
Requires:       binutils
Requires:       gcc
Requires:       make
Requires:       dkms
BuildRequires:  kernel-devel
BuildArch:      noarch

%description
Asus wmi device driver

%prep
%setup -q -n %{modname}-%{version}

%build

%install
rm -rf %{buildroot}

install -d -m 755 %{buildroot}/usr/src/%{modname}-%{version}-%{release}/src
install -m 644 Makefile %{buildroot}/usr/src/%{modname}-%{version}-%{release}/Makefile
install -m 644 src/%{modname}.c %{buildroot}/usr/src/%{modname}-%{version}-%{release}/src/%{modname}.c
install -m 644 src/asus-wmi.h %{buildroot}/usr/src/%{modname}-%{version}-%{release}/src/asus-wmi.h

#
cat > %{buildroot}/usr/src/%{modname}-%{version}-%{release}/dkms.conf << EOF
PACKAGE_NAME=%{modname}
PACKAGE_VERSION=%{version}-%{release}
MAKE[0]="make KVERSION=\$kernelver"
DEST_MODULE_LOCATION[0]="/updates/"
BUILT_MODULE_LOCATION[0]="src/"
BUILT_MODULE_NAME[0]=%{modname}
AUTOINSTALL="yes"
REMAKE_INITRD="yes"
NO_WEAK_MODULES="yes"
EOF
#
install -d -m 755 %{buildroot}/etc/modprobe.d
echo "blacklist asus-nb-wmi " > %{buildroot}%{_sysconfdir}/modprobe.d/asus-rog-nb-wmi.conf

%files
%dir /etc/modprobe.d
%dir /usr/src/%{modname}-%{version}-%{release}
/usr/src/%{modname}-%{version}-%{release}/*
%{_sysconfdir}/modprobe.d/asus-rog-nb-wmi.conf

%changelog
